 <!-- Our Service Section -->


 <section class="service full-height" id="service">
        <div class="container">
            <h2 class="text-center pb-5">Our Service</h2>
            <div class="row pb-3">
             

                <div class="col-lg-4 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="icon">
                                <i class="fa-solid fa-calendar-check"></i>
                            </div>
                            <h4 class="pb-2">Easy Appoinment</h4>
                            <p class="service-text">Conveniently book appointments online for easy and hassle-free scheduling.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="icon">
                                <i class="fa-solid fa-flask-vial"></i>
                            </div>
                            <h4 class="pb-2">Medical Test</h4>
                            <p class="service-text">Easily schedule medical tests online with our convenient appointment feature.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="icon">
                                <i class="fa-solid fa-file-medical"></i>
                            </div>
                            <h4 class="pb-2">Test Report</h4>
                            <p class="service-text">Access personalized test reports securely through our user-friendly website feature.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="icon">
                                <i class="fa-solid fa-house-medical"></i>
                            </div>
                            <h4 class="pb-2">24/7 Service</h4>
                            <p class="service-text">Experience round-the-clock medical services with our 24/7 availability feature.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="icon">
                                <i class="fa-solid fa-hospital"></i>
                            </div>
                            <h4 class="pb-2">Emergency Service</h4>
                            <p class="service-text">Receive immediate care with our emergency service available at all times.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 mb-3">
                    <div class="card text-center">
                        <div class="card-body">
                            <div class="icon">
                                <i class="fa-solid fa-truck-medical"></i>
                            </div>
                            <h4 class="pb-2">Ambulence Service</h4>
                            <p class="service-text">Swiftly reach medical assistance with our reliable ambulance service 24/7.</p>
                        </div>
                    </div>
                </div>
                

            </div>
        </div>
    </section>

    <!-- Our Service Section end -->